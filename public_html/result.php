<?php 
include 'models.php';
?>

<html>
	<head>
		<title> Search Engine </title>
						
		<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
						
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
<body>
<a href="index.php" class="btn btn-default btn-lg active" role="button">Search again</a>
<?php

if ($_POST['submit']) {
    $search = mysqli_real_escape_string($conn, $_POST['php_search']);

    if (empty($search)) {
        $message = '<h4>Please type a word in search field!</h4>';
        print ($message);
    } else {
        $message = '<h4>No match found!</h4>';
		
		$result = searchUsers($search, $conn);
		
        if (mysqli_num_rows($result) > 0) {
            $index = 0;
            $output .= '
			
			<center><h2> Search Results for: <i>"'. $search .'"</i></h2></center>
			<table class="table table-bordered table-striped"> 
				<thead> 
					<tr> 
						<th></th> 
						<th> First Name </th> 
						<th> Last Name</th> 
						<th> Favourite color</th> 
					</tr> 
				</thead> 
				<tbody> 
			';
            while ($row = mysqli_fetch_assoc($result)) {
                $index++;
				
                $output .= '
					<tr> 
						<th class="text-nowrap" scope="row">' . $index . '</th> 
						<td>' . $row["first_name"] . '</td> 
						<td>' . $row["last_name"] . '</td> 
						<td data-color="' . $row["color_title"] . '">' . $row["color_title"] . '</td> 
					</tr> 
				';
            }

            echo $output;
            ?>

				</tbody>
            </table>
           

            <?php
        } else {
            echo '	<center>
						<h2> Search Result</h2>
						<h3> '. $message .'</h3>
					</center>
			';
        }

        mysqli_free_result($result);
        mysqli_close($conn);
    }
}

?>
		</body>
    <script src="main.js"></script>
</html>

