<?php 
include 'models.php';

if (isset($_POST["query"])) {
    $search = mysqli_real_escape_string($conn, $_POST["query"]);
	
    $result = searchUsers($search, $conn);
	
    if (mysqli_num_rows($result) > 0) {
		$resultArray = mysqli_fetch_all($result, MYSQLI_ASSOC);

		$newFomatedArray = [];
		foreach($resultArray as $resultRow){
			$newFormatedRow = [];

			$newFormatedRow["first_name"] = $resultRow["first_name"];
			$newFormatedRow["last_name"] = $resultRow["last_name"];
			$newFormatedRow["color_title"] = $resultRow["color_title"];
			$newFomatedArray[] = $newFormatedRow;
		}

		$resultJson = json_encode($newFomatedArray);

		echo $resultJson;
    }
}


?>