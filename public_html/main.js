$( document ).ready(function() {
   
	function apply_color(){
		$("table tr td:last-child").dblclick(function() {
			$color = $(this).html();
			console.log($color);
			$("table tr").css('background','transparent')
			$(this).parent().css('background',$color);
		});
	}
	apply_color()
	
	$('.no-records-found td').hide();
	function get_data(query){
		$.ajax({
		   url: 'fetch.php',
		   dataType: 'json',
			type: 'POST',
			data: $.param({ query: query}),
		   success: function(data) {
			   $('#users').bootstrapTable('load', data);
			   apply_color()
		   },
		   error: function() {
			   $('#users').bootstrapTable('removeAll'); 
			   $('.no-records-found td').hide();
			   $('.no-records-found').append("<td colspan='4'>No records found!</td>");
		   }
		});
	}
	

	$('.search-engine').keyup(function(){
		var search = $(this).val();
		$('#live-search h2').show();
		$('#live-search h2 i').html(search);
		if(search != '') {
			get_data(search);
		} else {
			get_data();
		}
	});
	
});