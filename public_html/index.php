<html>
	<head>
		<title> Search Engine </title>
		
		<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>

		<!-- Latest compiled and minified Locales -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
		
	</head>
	<body>
		<form action="result.php" method="POST">
			<center><h3>Search Engine</h3></center>
			<center>
				<input class="search-engine" type="text" name="php_search" size="100" placeholder="ajax Search" autocomplete="off">
				<input type="submit" name="submit" value="PHP Search">
			</center>
		</form>
		<div id="live-search">
			<center><h2 style="display: none;"> Search Results for: <i></i></h2></center>
			<table class="table table-bordered table-striped" data-toggle="table" id="users"> 
				<thead> 
					<tr> 
						<th></th> 
						<th data-field="first_name"> First Name </th> 
						<th data-field="last_name"> Last Name</th> 
						<th data-field="color_title" data-class=""> Favourite color</th> 
					</tr> 
				</thead> 
			</table>
		</div>
		
	</body>
	<script src="main.js"></script>
</html>
