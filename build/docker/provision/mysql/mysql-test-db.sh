#!/bin/bash

# Creating test DB
#
# Variables:
#   db_user
#   db_password
#   db_name_test
#   db_user_test
#   db_pass_test
#
# Example:
#       /tmp/mysql-test-db.sh securephone securephone_password securephone_test securephone_test_user securephone_test_password

db_user=$1
db_password=$2
db_name_test=$3
db_user_test=$4
db_pass_test=$5

echo "[Mysql-Test-DB] [Step 1] Create test db"

mysql -u$db_user -p$db_password -e "CREATE DATABASE IF NOT EXISTS $db_name_test;" &>/dev/null

echo "[Mysql-Test-DB] [Step 2] Create test user"

mysql -u$db_user -p$db_password -e "CREATE USER '$db_user_test'@'%' IDENTIFIED BY '$db_pass_test';" &>/dev/null

echo "[Mysql-Test-DB] [Step 3] Grant access"

mysql -u$db_user -p$db_password -e "GRANT ALL PRIVILEGES ON $db_name_test. * TO {{mysql_user_test}}@'%';" &>/var/log/syslog
